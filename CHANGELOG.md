# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0] - 2023-07-14

### Added

- `StateMachine` now implements `System.IDisposable`
- `StateMachine.IState` can also implement `System.IDisposable`, this will also be called automatically.

## [1.1.0] - 2023-06-23

### Added

- Enable verbose logging with scripting define
- GetState<T> to get specific state instance

## [1.0.0] - 2023-01-10

### Added

- StateMachine and StateMachine.IState

[unreleased]: https://gitlab.com/tidakjelas92/unity-fsm/-/compare/v1.2.0...main
[1.2.0]: https://gitlab.com/tidakjelas92/unity-fsm/-/tags/v1.2.0
[1.1.0]: https://gitlab.com/tidakjelas92/unity-fsm/-/tags/v1.1.0
[1.0.0]: https://gitlab.com/tidakjelas92/unity-fsm/-/tags/v1.0.0
