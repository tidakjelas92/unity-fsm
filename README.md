# unity-fsm

Finite State Machine framework to help build various state machines. Use as a package in Unity.

## How to install

1. Open Package Manager
2. Add Package > from git url
3. Paste the link `https://gitlab.com/tidakjelas92/unity-fsm.git`
4. Reference the asmdef (if your project uses asmdef)

## How to use

1. Instantiate new `StateMachine`
2. Define states that derive from `StateMachine.IState`.
3. Populate the `StateMachine` with created states.
4. Call `StateMachine.Update()` when the state machine needs to update.
5. Dispose the `StateMachine` properly

### Instantiate new StateMachine

```csharp
using TidakJelas.FSM;

public class Fish : MonoBehaviour
{
    private StateMachine _stateMachine;

    private void Awake()
    {
        _stateMachine = new StateMachine("FishStateMachine");
    }
}
```

### Define States

```csharp
using System;
using TidakJelas.FSM;
using UnityEngine;

public class Fish : MonoBehaviour
{
    // Initialization codes...

    private class Idle : StateMachine.IState
    {
        public Type NextState => typeof(Move);
        public void OnEnter() => Debug.Log("fish enter idle state.");
        public void OnUpdate() => Debug.Log("fish on idle update state.");
        public void OnExit() => Debug.Log("fish exit idle state.");
    }

    private class Move : StateMachine.IState, IDisposable
    {
        public Type NextState => typeof(Idle);
        public void OnEnter() => Debug.Log("fish enter move state.");
        public void OnUpdate() => Debug.Log("fish on move update state.");
        public void OnExit() => Debug.Log("fish exit move state.");
        public void Dispose() => Debug.Log("Dispose move state!");
    }
}
```

### Populate StateMachine with created states.

```csharp
using System;
using TidakJelas.FSM;
using UnityEngine;

public class Fish : MonoBehaviour
{
    private StateMachine _stateMachine;

    private void Awake()
    {
        _stateMachine = new StateMachine();
    }

    private void Start()
    {
        _stateMachine.AddStates(new StateMachine.IState[] {
            new Idle(),
            new Move()
        });
        _stateMachine.SetStartingState(typeof(Idle));
    }

    // Fish states code...
}
```

### Call Update

```csharp
using System;
using TidakJelas.FSM;
using UnityEngine;

public class Fish : MonoBehaviour
{
    // Initialization code...

    private void Update()
    {
        _stateMachine.Update();
    }

    // Fish states code...
}
```

### Dispose `StateMachine`

```csharp
using System;
using TidakJelas.FSM;
using UnityEngine;

public class Fish : MonoBehaviour
{
    // Other codes omitted...

    private void OnDestroy()
    {
        _stateMachine.Dispose();
        _stateMachine = null;
    }
}
```

## Verbose Logging

Define `TIDAKJELAS_FSM_VERBOSE` with global Player settings, or with `csc.rsp`. See [link](https://docs.unity3d.com/Manual/CustomScriptingSymbols.html)

Currently, the state machine logs every state enter and exit. Update is not logged because it is called every frame. Logging is formatted this way:

```csharp
$"<b>[{_id}]</b> Entering state: {currentStateType}"
$"<b>[{_id}]</b> Exiting state: {currentStateType}"
```

**[ApplicationState]** Entering state: MainMenuState
