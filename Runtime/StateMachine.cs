using System;
using System.Collections.Generic;
#if TIDAKJELAS_FSM_VERBOSE
using UnityEngine;
#endif

namespace TidakJelas.FSM
{
    public class StateMachine : IDisposable
    {
        private static int _unnamedIdCount;
        private IState _currentState;
        private readonly string _id;

        public Type CurrentState => _currentState.GetType();

        private Dictionary<Type, IState> _states = new Dictionary<Type, IState>();

        public StateMachine()
        {
            _id = _unnamedIdCount.ToString();
            _unnamedIdCount += 1;
        }

        /// <param name="id">id is used for verbose logging</param>
        public StateMachine(string id)
        {
            _id = id;
        }

        public void Update()
        {
            var nextStateType = _currentState?.NextState;
            SetState(nextStateType);
            _currentState?.OnUpdate();
        }

        public void AddState(IState state) => _states.Add(state.GetType(), state);

        public void AddStates(IState[] states)
        {
            foreach (var state in states)
                AddState(state);
        }

        public T GetState<T>() => (T)_states[typeof(T)];

        public void SetStartingState(Type stateType)
        {
            _currentState = _states[stateType];
            _currentState.OnEnter();
        }

        public void SetState(Type nextStateType)
        {
            var currentStateType = _currentState.GetType();
            if (currentStateType == nextStateType)
                return;

#if TIDAKJELAS_FSM_VERBOSE
            Debug.Log($"<b>[{_id}]</b> Exiting state: {currentStateType}");
#endif
            _currentState.OnExit();
            _currentState = _states[nextStateType];

#if TIDAKJELAS_FSM_VERBOSE
            Debug.Log($"<b>[{_id}]</b> Entering state: {_currentState.GetType()}");
#endif
            _currentState.OnEnter();
        }

        public void Dispose()
        {
#if TIDAKJELAS_FSM_VERBOSE
            Debug.Log($"<b>[{_id}]</b> Disposing state machine.");
#endif
            foreach (var (key, state) in _states)
            {
                var disposable = state as IDisposable;
                disposable?.Dispose();
            }
        }

        public interface IState
        {
            Type NextState { get; }

            void OnEnter();
            void OnUpdate();
            void OnExit();
        }
    }
}
